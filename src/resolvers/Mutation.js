function postUser(parent, args, context, info) {
    return context.prisma.createUser({
        name: args.name
    })
}

async function postMessage(parent, args, context, info){
    const userExists = await context.prisma.$exists.user({
        id: args.userId
    });

    if(!userExists){
        throw new Error(`User with ID ${args.userId} does not exist`);
    }
    return context.prisma.createMessage({
        userId: args.userId,
        text: args.text,
        likes: args.likes,
        dislikes: args.dislikes,
        user: { connect: {id: args.userId}}
    });
}

async function postReply(parent, args, context, info){
    const messageExists = await context.prisma.$exists.message({
        id: args.messageId
    });

    if(!messageExists){
        throw new Error(`Message with ID ${args.messageId} does not exist`);
    }
    return context.prisma.createReply({
        messageId: args.messageId,
        text: args.text,
        message: { connect: {id: args.messageId}}
    });
}

module.exports = {
    postMessage,
    postUser,
    postReply
}