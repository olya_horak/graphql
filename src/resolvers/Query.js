async function messages(parent, args, context) {
    const messages = await context.prisma.messages();
    return messages;
}

async function users(parent, args, context) {
    const users = await context.prisma.users();
    return users;
}

async function replies(parent, args, context) {
    const replies = await context.prisma.replies();
    return replies;
}

module.exports = {
    messages,
    users,
    replies
}