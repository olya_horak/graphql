function user(parent, args, context) {
    return context.prisma.message({
        id: parent.id
    }).user();
}

function replies(parent, args, context){
    return context.prisma.message({
        id: parent.id
    }).replies();
}


module.exports = {
    user,
    replies
}